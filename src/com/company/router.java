import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Sameer on 10/4/2015.
 */
public class Router {
    /**
     * Current address of the Router
     */
    int selfAddress;
    /**
     * List of its neighbors
     */
    ArrayList<Integer> neighbours;
    /**
     * List of subnets attached to this rotuer
     */
    ArrayList<String> networks;
    /**
     * Routing table of the current Router
     */
    RoutingTable table;
    /**
     * HashMap maintaining last
     */
    HashMap<Integer,Long> lastKnown = new HashMap<Integer,Long>();

    /**
     * Constructor for Router
     * Each Router has a address and a list of neighbors
     * @param selfAddress Address of current Router
     * @param numberOfNeighbors Number of neighbors
     * @param neighbours Addresses of the neighbors
     */
    public Router(int selfAddress, int numberOfNeighbors, ArrayList<Integer> neighbours, ArrayList<String> networks) {
        this.selfAddress = selfAddress;
        this.neighbours = new ArrayList<Integer>(neighbours);
        this.networks = new ArrayList<String>(networks);
        this.bootRouter(networks);
    }

    /**
     * @Override
     * Prints Router config
     * @return
     */
    public String toString() {
        return "\n"+this.table;
    }

    /**
     * Broadcasts its table to all its neighbours
     */
    public void broadcastTable() {
        invalidateExpiredEntires();
        DatagramSocket datagramSocket;
        try {
            datagramSocket = new DatagramSocket();
        }catch(SocketException e) {
            return;
        }
        InetAddress receiverAddress = null;
        try {
            receiverAddress  = InetAddress.getLocalHost();
        }catch(UnknownHostException e) {
            return;
        }
        byte[] buffer = toBytes(this.table);
        for(Integer i : this.neighbours) {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress, i);
            try {
                datagramSocket.send(packet);
                Thread.sleep(500);
            } catch(IOException e) {
            } catch (InterruptedException e) {

            }
        }
    }

    /**
     * Updates table on the receipt of a neighboring table.
     * @param neighbor RoutingTable Table from the neighbor
     */
    public void updateTable(RoutingTable neighbor) {
        boolean isUpdated = false;
        this.lastKnown.put(neighbor.address,new Date().getTime());
        for(String key : neighbor.tableLookup.keySet()) {
                if(!this.table.tableLookup.containsKey(key)) {
                    int c = 1 ;
                    int dv = neighbor.tableLookup.get(key).distance;
                    this.table.tableLookup.put(key, new TableData(c+dv,neighbor.address));
                    isUpdated = true;
                }else {
                    int dy = this.table.tableLookup.get(key).distance;
                    int dv = neighbor.tableLookup.get(key).distance;
                    if (dy < 16 && dv < 16) {
                        int c = 1;
                        if (c + dv <= dy) {
                            this.table.tableLookup.put(key, new TableData(c + dv, neighbor.address));
                            isUpdated = true;
                        }
                    }else {
                        this.table.tableLookup.put(key, new TableData(16, neighbor.address));
                        isUpdated = true;
                    }
                }
        }
        printTable(isUpdated);
    }

    /**
     * Converts a RoutingTable object to a byte array.
     * @param table RoutingTable Table object
     * @return byte[] Byte stream
     */
    public byte[] toBytes(RoutingTable table) {
        // Serialize to a byte array
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        try {
            ObjectOutput oo = new ObjectOutputStream(bStream);
            oo.writeObject(table);
            oo.flush();
            oo.close();
        }catch(IOException e) {
        }
        return  bStream.toByteArray();
    }

    /**
     * Prints routing table
     * @param isUpdated Boolean Flag indicating whether the table was udpated.
     */
    public void printTable(boolean isUpdated) {
        if(isUpdated) {
            System.out.println();
            System.out.println("Printing table for Router: " + intToIp(this.table.address));
            System.out.println("Address                    Next Hop              Cost     ");
            System.out.println("=====================================================");
            for(String s : this.table.tableLookup.keySet()) {
                System.out.println(s + "         " + intToIp(this.table.tableLookup.get(s).via) + "         " +this.table.tableLookup.get(s).distance  );
            }
            System.out.println();
            System.out.println("Press 1 to enter new/existing neighbour");
            System.out.println("Press 2 to enter new/existing network");
            System.out.println();
        }
    }

    /**
     * Boots current Router. Initializes all the
     * @param networks List of networks the current Router is connected to
     */
    public void bootRouter(ArrayList<String> networks) {
        System.out.println("Booting Router");
        this.table = new RoutingTable(this.selfAddress,populateTable(networks));
        printTable(true);
        System.out.println("Router successfully booted");
    }

    /**
     * Initialises the routing table with the currently attacked networks
     * @param networks List of networks the current
     * @return HashMap of the routing table
     */
    public HashMap<String, TableData> populateTable(ArrayList<String> networks) {
        HashMap<String, TableData> tmp = new HashMap<String, TableData>();
        for(String i : this.networks) {
            tmp.put(i,new TableData(0,this.selfAddress));
        }
        return tmp;
    }

    /**
     * Converts a stream of bytes into RoutingTable object
     * @param buffer Stream of bytes
     * @return
     */
    public RoutingTable toRoutingTable(byte[] buffer) {
        //start of receive object
        RoutingTable neighborTable = null;
        try {
            ObjectInputStream iStream = new ObjectInputStream(new ByteArrayInputStream(buffer));
            neighborTable = (RoutingTable) iStream.readObject();
            iStream.close();
        }catch(IOException e) {

        }catch (ClassNotFoundException e) {

        }
        //end of receive object
        return neighborTable;
    }

    /**
     * Checks timestamps of the neighbors to see if any one of them are offline
     */
    public void invalidateExpiredEntires() {
        boolean isUpdated = false;
        for(Integer i : this.lastKnown.keySet()) {
            if( (new Date().getTime() - this.lastKnown.get(i)) >= 6000) {
                isUpdated = true;
                for(String s : this.table.tableLookup.keySet()) {
                    if(this.table.tableLookup.get(s).via == i) {
                        TableData tmp = this.table.tableLookup.get(s);
                        tmp.distance = 16;
                        this.table.tableLookup.put(s,tmp);
                    }
                }
            }
        }
        printTable(isUpdated);
    }

    /**
     * Converts IP address to a int format for representing ports
     * @param input String representing the IP address in xxx.xxx.xxx.xxx format
     * @return int value of the ip address
     */
    public int ipToInt(String input) {
        String subParts[] = input.split("[.]");
        String thirdPart = padZeros(Integer.toBinaryString(Integer.parseInt(subParts[2])),8);
        String fourthPart = padZeros(Integer.toBinaryString(Integer.parseInt(subParts[3])),8);
        return  Integer.parseInt(thirdPart + fourthPart,2);
    }

    /**
     * Pads zeros to a given string
     * @param input Input String to be padded
     * @param length Desired length of the string
     * @return Padded string
     */
    public String padZeros(String input,int length) {
        while(input.length() != length) {
            input = "0" + input;
        }
        return input;
    }

    /**
     * Converts int value to IP address notation
     * @param input Int value of the port
     * @return String IP address notation
     */
    public String intToIp(int input) {
        String binary = padZeros("" + Integer.toBinaryString(input),16);
        int thirdFragment = Integer.parseInt(binary.substring(0, 8), 2);
        int fourthFragment = Integer.parseInt(binary.substring(8),2);
        return "000.000."+padZeros(thirdFragment+"",3)+"."+padZeros(fourthFragment+"",3);
    }

    /**
     * Starts boradcasting in a infinite loop
     */
    public void startBroadcasting() {
        while(true) {
            this.broadcastTable();
            try {
                Thread.sleep(1000);
            }catch (InterruptedException e){
            }
        }
    }

    /**
     * Starts receiving thread in a infinite loop
     */
    public void startReceiving() {
        byte[] buffer = null;
        DatagramSocket datagramSocket = null;
        try {
            datagramSocket = new DatagramSocket(this.selfAddress);
        }catch(SocketException e) {
        }
        try {
            buffer = new byte[datagramSocket.getReceiveBufferSize()];
        }catch(SocketException e) {
        }
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        while(true) {
            try {
                datagramSocket.receive(packet);
                RoutingTable neighborTable = this.toRoutingTable(buffer);
                if(neighborTable != null) {
                    this.updateTable(neighborTable);
                }
            }catch (java.net.SocketTimeoutException e) {
            }
            catch(IOException e) {
            }
        }
    }
}
