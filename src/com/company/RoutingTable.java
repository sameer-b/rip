import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Sameer on 10/5/2015.
 */
public class RoutingTable implements Serializable {
    /**
     * Current address of the Router
     */
    public int address;
    /**
     * Lookup table for the current Router
     */
    public HashMap<String, TableData> tableLookup;

    /**
     * Constructor for RoutingTable
     * @param address Address of the current Router
     * @param table HashMap of the entries
     */
    public RoutingTable(int address,HashMap<String, TableData> tableLookup) {
        this.address = address;
        this.tableLookup = new HashMap<String, TableData>(tableLookup);
    }
}
