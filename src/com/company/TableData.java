import java.io.Serializable;

/**
 * Created by Sameer on 10/5/2015.
 */
public class TableData implements Serializable{
    /**
     * Distance of the subnet
     */
    int distance;
    /**
     * Next Hop
     */
    int via;

    /**
     * Constuctor for TableData
     * @param distance
     * @param via
     */
    public TableData(int distance, int via) {
        this.distance = distance;
        this.via = via;
    }
}
