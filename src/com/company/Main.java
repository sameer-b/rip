import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        final Router r = readConfigAndBootRouter(args[0]);

        /**
         * Create thread for broadcasting current RoutingTable
         */
        new Thread() {
            public void run() {
                r.startBroadcasting();
            }
        }.start();

        /**
         * Create thread for receiving updates from neighbors
         */
        new Thread() {
            public void run() {
                r.startReceiving();
            }
        }.start();

        /**
         * Wait for user input
         */
        while(true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int c = Integer.parseInt(br.readLine());
            switch (c) {
                case 1: System.out.println("Enter new/existing neighbour in the form: <neighbor address> <cost>");
                    break;
                case 2: System.out.println("Enter new/existing network in the form: <subnet address> <cost>");
                    break;
            }
        }
    }

    /**
     * Reads config file and constructs a Router
     * @param fileName Name of the config file
     * @return Router object
     * @throws FileNotFoundException
     */
    public static Router readConfigAndBootRouter(String fileName) throws FileNotFoundException {
        Scanner in = new Scanner(new FileReader(fileName));
        ArrayList<Integer> neighbors = new ArrayList<>();
        ArrayList<String> networks = new ArrayList<>();
        int selfAddress = 0;
        while(in.hasNextLine()) {
            String line = in.nextLine();
            if(line.contains("ADDRESS")) {
                selfAddress = Integer.parseInt(line.replace("ADDRESS:","").trim());

            }else if(line.contains("NEIGHBOR")) {
                neighbors.add(Integer.parseInt(line.replace("NEIGHBOR:","").trim()));
            }else if(line.contains("NETWORK")){
                networks.add(line.replace("NETWORK:","").trim());
            }
        }
        return new Router(selfAddress,neighbors.size(),neighbors,networks);
    }
}
